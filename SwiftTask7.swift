// Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать
// марку авто, год выпуска, объем багажника/кузова, запущен ли двигатель, открыты ли окна, заполненный объем багажника
// Описать перечисление с возможными действиями с автомобилем: запустить/заглушить двигатель, 
// открыть/закрыть окна, погрузить/выгрузить из кузова/багажника груз определенного объема
// Добавить в структуры метод с одним аргументом типа перечисления, который будет менять свойства структуры в зависимости от действия

enum MachineAction {
	case engineStart 
	case engineStop 
	case windowsOpen
	case windowsClose 
	case trunkLoad 
	case trunkUnload
}

struct Car {
	let model: String
	let year: Int 
	var isEngineStarted: Bool = false
	var trunkCapasity: Int = 30
	var isWindowsOpen: Bool = false
	var currentTrunkVolume: Int = 0

	mutating func doAction(machineAction: MachineAction) {
		switch machineAction {
			case .engineStart: 
				isEngineStarted = true
			case .engineStop: 
				isEngineStarted = false
			case .windowsOpen: 
				isWindowsOpen = true
			case .windowsClose: 
				isWindowsOpen = false
			case .trunkLoad: 
				if (currentTrunkVolume < trunkCapasity) {
					currentTrunkVolume += 1
				}
			case .trunkUnload: 
				if (currentTrunkVolume > 0) {
					currentTrunkVolume -= 1
				}
		}
	}
}

// Инициализировать несколько экземпляров структур. Применить к ним различные действия. Положить объекты 
// структур в словарь как ключи, а их названия как строки например var dict = [structCar: 'structCar']

var car1 = Car(model: "Audi", year: 2010, trunkCapasity: 40, currentTrunkVolume: 15)
car1.doAction(machineAction: MachineAction.trunkLoad)
car1.doAction(machineAction: MachineAction.engineStart)
var car2 = Car(model: "Bmw", year: 2015, isEngineStarted: true, trunkCapasity: 35)
car2.doAction(machineAction: MachineAction.windowsOpen)

var dict: [String : Any] = [:]
dict[car1.model] = car1
dict[car2.model] = car2

// Исправить фрагменты кода:

class Car {
	weak var driver: Man?
	deinit { //выведем в консоль сообщение о том что объект удален print("машина удалена из памяти")
		print("машина удалена из памяти")
	}
}

class Man {
	weak var myCar: Car?
	deinit{ //выведем в консоль сообщение о том что объект удален
		print("мужчина удален из памяти")
	}
}

//Обхявим переменные как опциональные, что бы иметь возможность присвоить им nil
var car: Car? = Car()
var man: Man? = Man()
//машина теперь имеет ссылку на мужчину
car?.driver = man
//а мужчина на машину
man?.myCar = car
//присвоим nil переменным, удалим эти ссылки
car = nil
man = nil
//мы больше не можем никак обратится к нашим объектам, но они продолжают существовать в памяти

class Man {
	var pasport: Passport? // По заданию weak или unowned сюда запрещено ставить!
	deinit { // выведем в консоль сообщение о том, что объект удален 
		print("мужчина удален из памяти")
	}
}

class Passport {
	unowned let man: Man
	init(man: Man) {
		self.man = man
	}
	deinit { // выведем в консоль сообщение о том, что объект удален
		print("паспорт удален из памяти")
	}
}
var man: Man? = Man()
var passport: Passport? = Passport(man: man!) 
man?.pasport = passport 
passport = nil // объект еще не удален, его удерживает мужчина 
man = nil // теперь удалены оба объекта






